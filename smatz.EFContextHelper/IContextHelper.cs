﻿using System;
using System.Data.Entity;

namespace smatz.EFContextHelper
{
    /// <summary>
    /// Main interface from EFContextHelper
    /// </summary>
    public interface IContextHelper
    {
        /// <summary>
        /// Executes an function on an DbContext and returns value of type T
        /// </summary>
        /// <typeparam name="T">Type of the result from work</typeparam>
        /// <param name="work">will execute on an DbContext and returns T</param>
        /// <returns></returns>
        T ExecuteOnContext<T>(Func<DbContext, T> work);

        /// <summary>
        /// Executes an function on an DbContext and returns value of type T
        /// LazyLoading, Tracking and Proxy is set to FALSE
        /// </summary>
        /// <typeparam name="T">Type of the result from work</typeparam>
        /// <param name="work">will execute on an DbContext and returns T</param>
        /// <returns></returns>
        T ExecuteOnContextReadOnly<T>(Func<DbContext, T> work);

        /// <summary>
        /// Executes an function on an DbContext and returns value of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="work">will execute on an DbContext and returns T</param>
        /// <param name="lazyLoading">Enables or disables Lazy loading on DbContext</param>
        /// <param name="tracking">Enables or disables change detection on DbContext</param>
        /// <param name="proxy">Enables or disables proxy creation on DbContext</param>
        /// <returns></returns>
        T ExecuteOnContext<T>(Func<DbContext, T> work, bool lazyLoading, bool tracking, bool proxy);

        /// <summary>
        /// Executes an function on an DbContext in an transaction and returns value of type T
        /// Function has to call Commit on transaction
        /// </summary>
        /// <typeparam name="T">Type of the result from work</typeparam>
        /// <param name="work">will execute on an DbContext and returns T</param>
        /// <returns></returns>
        T ExecuteOnContextInTransaction<T>(Func<DbContext, DbContextTransaction, T> work);

        /// <summary>
        /// Executes an function on an DbContext and returns value of type T
        /// </summary>
        /// <typeparam name="T">Type of the result from work</typeparam>
        /// <param name="work">will execute on an DbContext and returns T</param>
        /// <param name="lazyLoading">Enables or disables Lazy loading on DbContext</param>
        /// <param name="tracking">Enables or disables change detection on DbContext</param>
        /// <param name="proxy">Enables or disables proxy creation on DbContext</param>
        /// <returns></returns>
        T ExecuteOnContextInTransaction<T>(Func<DbContext, DbContextTransaction, T> work, bool lazyLoading, bool tracking, bool proxy);

        /// <summary>
        /// Executes an action on an DbContext
        /// </summary>
        /// <param name="work">will execute on an DbContext</param>
        void ExecuteOnContext(Action<DbContext> work);

        /// <summary>
        /// Executes an action on an DbContext
        /// </summary>
        /// <param name="work">will execute on an DbContext</param>
        /// <param name="lazyLoading">Enables or disables Lazy loading on DbContext</param>
        /// <param name="tracking">Enables or disables change detection on DbContext</param>
        /// <param name="proxy">Enables or disables proxy creation on DbContext</param>
        void ExecuteOnContext(Action<DbContext> work, bool lazyLoading, bool tracking, bool proxy);

        /// <summary>
        /// Executes an action on an DbContext
        /// </summary>
        /// <param name="work">will execute on an DbContext</param>
        void ExecuteOnContextInTransaction(Action<DbContext, DbContextTransaction> work);

        /// <summary>
        /// Executes an action on an DbContext
        /// </summary>
        /// <param name="work">will execute on an DbContext</param>
        /// <param name="lazyLoading"></param>
        /// <param name="tracking">Enables or disables change detection on DbContext</param>
        /// <param name="proxy">Enables or disables proxy creation on DbContext</param>
        void ExecuteOnContextInTransaction(Action<DbContext, DbContextTransaction> work, bool lazyLoading, bool tracking, bool proxy);

        /// <summary>
        /// Delivers an DbContext to work on, can be injected through dependency injection, e.g. Castle Windsor
        /// </summary>
        IContextProvider ContextProvider { get; set; }
    }

}
