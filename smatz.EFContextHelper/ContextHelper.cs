﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smatz.EFContextHelper
{
    public class ContextHelper : IContextHelper
    {
        #region Injections
        public IContextProvider ContextProvider { get; set; }
        #endregion

        #region IDbContextHelper Implementations

        public T ExecuteOnContext<T>(Func<DbContext, T> work)
        {
            return this.ExecuteOnContext(work, true, true, true);
        }

        public T ExecuteOnContextReadOnly<T>(Func<DbContext, T> work)
        {
            return this.ExecuteOnContext(work, false, false, false);
        }

        public T ExecuteOnContext<T>(Func<DbContext, T> work, bool lazyLoading, bool tracking, bool proxy)
        {
            using (var db = this.ContextProvider.GetContext())
            {
                db.Configuration.LazyLoadingEnabled = lazyLoading;
                db.Configuration.ProxyCreationEnabled = proxy;
                db.Configuration.AutoDetectChangesEnabled = tracking;
                return work(db);
            }
        }

        public void ExecuteOnContext(Action<DbContext> work)
        {
            this.ExecuteOnContext(work, true, true, true);
        }

        public void ExecuteOnContext(Action<DbContext> work, bool lazyLoading, bool tracking, bool proxy)
        {
            using (var db = this.ContextProvider.GetContext())
            {
                db.Configuration.LazyLoadingEnabled = lazyLoading;
                db.Configuration.ProxyCreationEnabled = proxy;
                db.Configuration.AutoDetectChangesEnabled = tracking;
                work(db);
            }
        }

        public T ExecuteOnContextInTransaction<T>(Func<DbContext, DbContextTransaction, T> work)
        {
            return this.ExecuteOnContextInTransaction(work, true, true, true);
        }

        public T ExecuteOnContextInTransaction<T>(Func<DbContext, DbContextTransaction, T> work, bool lazyLoading, bool tracking, bool proxy)
        {
            using (var db = this.ContextProvider.GetContext())
            {
                db.Configuration.LazyLoadingEnabled = lazyLoading;
                db.Configuration.ProxyCreationEnabled = proxy;
                db.Configuration.AutoDetectChangesEnabled = tracking;

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        return work(db, transaction);
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void ExecuteOnContextInTransaction(Action<DbContext, DbContextTransaction> work)
        {
            this.ExecuteOnContextInTransaction(work, true, true, true);
        }

        public void ExecuteOnContextInTransaction(Action<DbContext, DbContextTransaction> work, bool lazyLoading, bool tracking, bool proxy)
        {
            using (var db = this.ContextProvider.GetContext())
            {
                db.Configuration.LazyLoadingEnabled = lazyLoading;
                db.Configuration.ProxyCreationEnabled = proxy;
                db.Configuration.AutoDetectChangesEnabled = tracking;

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        work(db, transaction);
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        #endregion
    }
}
