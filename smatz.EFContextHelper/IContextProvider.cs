﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smatz.EFContextHelper
{
    /// <summary>
    /// Delivers an DbContext to use together with EFContextHelper
    /// No  implementation in the package, you have to implement this interface on your own
    /// </summary>
    public interface IContextProvider
    {
        /// <summary>
        /// Delivers a new DbContext
        /// </summary>
        /// <returns></returns>
        DbContext GetContext();
    }

}
