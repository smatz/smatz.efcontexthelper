# smatz.EFContextHelper

Small Helper for DbContext to execute code on DbContext and decouple context creation.

## Usage

I always wanted to decouple the creation of DbContext from using DbContext, so I developed this two interfaces and one implementation.
First of all it's necessary to implement the interface *IContextProvider*. The package itself does not contain a implementation of it.

**Sample of implementing IContextProvider**


    public class ContextProvider : IContextProvider
    {
	    public DbContext GetContext()
	    {
		    // create your dbcontext here
		    ...
	    }
    }


After implementation of the *IContextProvider* the *IContextHelper* is ready for usage.
First choice is to use an Property Dependency Injection Container like [Castle Windsor](http://www.castleproject.org/projects/windsor/) or just set the Property *ContextProvider* on the *ContextHelper*.

**After that, it is possible to use the context with one of the methods, e.g.:**
```
IContextHelper contextHelper = new ContextHelper(); // Better would be dependency injection!
contextHelper.ContextProvider = [yourContextProvider]; // Better would be dependency injection!

var result = contextHelper.ExecuteOnContext(db =>
{
	// do whatever yout want with the context in db
	// you can return something or just void
	...
});
```